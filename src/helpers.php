<?php

if (!function_exists('base_path')) {

	function base_path($path = '') {
		return app()->basePath() . ($path ? DIRECTORY_SEPARATOR . $path : $path);
	}
}

if (!function_exists('app_path')) {

	function app_path($path = '') {
		return app('path') . ($path ? DIRECTORY_SEPARATOR . $path : $path);
	}
}

if (!function_exists('config_path')) {

	function config_path($path = '') {
		return base_path('config') . ($path ? DIRECTORY_SEPARATOR . $path : $path);
	}
}
