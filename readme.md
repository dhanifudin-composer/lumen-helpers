# Lumen Helpers

Laravel lovers, but want to use Lumen.

# Install

First, add repositories into your composer.json then run ```composer update```.
More info you can look [here](http://dhanifudin.bitbucket.org/packagist).

```json
{
	"repositories": [
		{
			"type": "composer",
			"url": "http://dhanifudin.bitbucket.org/packagist"
		}
	],

	"require": {
		"dhanifudin/lumen-helpers": "1.0"
	}
}
```

# Features
* ```base_path()```
* ```app_path()```
* ```config_path()```
